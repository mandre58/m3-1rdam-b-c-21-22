
import java.util.Scanner;
public class EstacionsDelAny {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int dia = 0;
		char mes;
		String estacio = null;
		System.out.print("Introdueix el dia: ");
		dia = reader.nextInt();
		System.out.print("Introdueix el mes: ");
		mes = reader.next().charAt(0);
		mes = Character.toUpperCase(mes);
		if(dia < 1 || dia > 31) {
			System.out.print("Dia incorrecte.");
		} else {
			switch(mes)
			{
			case 'G':
				estacio = "Hivern";
				break;
			case 'F':
				estacio = "Hivern";
				break;
			case 'M':
				if(dia < 20) {
					estacio = "Hivern";
				} else {
					estacio = "Primavera";
				}
				break;
			case 'A':
				estacio = "Primavera";
				break;
			case 'I':
				estacio = "Primavera";
				break;
			case 'J':
				if(dia < 22) {
					estacio = "Primavera";
				} else {
					estacio = "Estiu";
				}
				break;
			case 'U':
				estacio = "Estiu";
				break;
			case 'T':
				estacio = "Estiu";
				break;
			case 'S':
				if(dia < 22) {
					estacio = "Estiu";
				} else {
					estacio = "Tardor";
				}
				break;
			case 'O':
				estacio = "Tardor";
				break;
			case 'N':
				estacio = "Tardor";
				break;
			case 'D':
				if(dia < 21) {
					estacio = "Tardor";
				} else {
					estacio = "Hivern";
				}
				break;
			default:
				estacio = "LLetra del mes incorrecte.";
			}
			System.out.println(estacio);
			reader.close();
		}
	}
}