import java.util.Scanner;
//examen 1r parc 2021/22- Passar a 2nDAM/

public class Passar2nDAM {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner(System.in);
		double percentAprovat = 0.0;
		final int TOTALHORES = 823; // hores totals curs de 1r DAM
		int horesAprovades = 0; 
		char M1; // 132 hsss
		char M2; // 165 h
		char M3; // 229 h
		char M4; // 99 h
		char M10; // 66 h
		char FOL; // 132 h

		System.out.println("Has aprovat M1-SO? respon 'S' o 'N' :");
		M1 = lector.nextLine().charAt(0);
		M1 = Character.toUpperCase(M1);
		System.out.println("Has aprovat M2-BD? respon 'S' o 'N' :");
		M2 = lector.nextLine().charAt(0);
		M2 = Character.toUpperCase(M2);
		System.out.println("Has aprovat M3+M5-Prog? respon 'S' o 'N' :");
		M3 = lector.nextLine().charAt(0);
		M3 = Character.toUpperCase(M3);
		System.out.println("Has aprovat M4-XML? respon 'S' o 'N' :");
		M4 = lector.nextLine().charAt(0);
		M4 = Character.toUpperCase(M4);
		System.out.println("Has aprovat M10-ERPs? respon 'S' o 'N' :");
		M10 = lector.nextLine().charAt(0);
		M10 = Character.toUpperCase(M10);
		System.out.println("Has aprovat FOL? respon 'S' o 'N' :");
		FOL = lector.nextLine().charAt(0);
		FOL = Character.toUpperCase(FOL);
		if (M1 != 'S' && M1 != 'N' || M2 != 'S' && M2 != 'N' || M3 != 'S' && M3 != 'N' || M4 != 'S' && M4 != 'N'
				|| M10 != 'S' && M10 != 'N' || FOL != 'S' && FOL != 'N') {
			System.out.println("Resposta incorrecta, respon sempre 'S' o 'N' :");
		} else {
			if (M1 == 'S') {
				horesAprovades += 132;
			}
			if (M2 == 'S') {
				horesAprovades += 165;
			}
			if (M3 == 'S') {
				horesAprovades += 229;
			}
			if (M4 == 'S') {
				horesAprovades += 99;
			}
			if (M10 == 'S') {
				horesAprovades += 66;
			}
			if (FOL == 'S') {
				horesAprovades += 132;
			}

			percentAprovat = horesAprovades * 100 / TOTALHORES;
			System.out.println(horesAprovades);

			if (percentAprovat >= 60) {
				System.out.println("S promociones a 2n, has aprovat el " + percentAprovat);
				if (percentAprovat >= 80 && M2 == 'S' && M3 == 'S') {
					System.out.println("I s podrs fer FCT");
				} else {
					System.out.println("Per no podrs fer FCT");
				}
			} else {
				System.out.println("No promociones a 2n, noms has aprovat el " + percentAprovat);
			}
		}
		lector.close();
	}
}
