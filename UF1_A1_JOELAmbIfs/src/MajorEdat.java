import java.util.Scanner;

public class MajorEdat {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner lector = new Scanner (System.in);
		
		int dNa, mNa, aNa, dAc, mAc, aAc;
		System.out.println("Introdueix el dia de naixement ");
		dNa = lector.nextInt();
		System.out.println("Introdueix el mes de naixement ");
		mNa = lector.nextInt();
		System.out.println("Introdueix l'any de naixement ");
		aNa = lector.nextInt();
		System.out.println("Introdueix el dia d'avui ");
		dAc = lector.nextInt();
		System.out.println("Introdueix el mes d'avui ");
		mAc = lector.nextInt();
		System.out.println("Introdueix l'any d'avui ");
		aAc = lector.nextInt();
		
		if (aAc - aNa > 18  || aAc - aNa == 18 && mAc - mNa > 0
				|| aAc - aNa == 18 && mAc == mNa && dAc - dNa >= 0) {
			System.out.println("Ja ets major d'edat");
		} else {
			System.out.println("Encara no ets major d'edat");
		}
		lector.close();
	}

}
