import java.util.Scanner;

public class ButlletiNotes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner lector = new Scanner (System.in);
		System.out.println("Introdueix la qualificaci�: ");
		
		int n = lector.nextInt();
		
		if (n < 0 || n > 10)  //validem les dades d'entrada
			System.out.println("Error nota incorrecta");
		else {		
			if (n >= 0 && n <= 4)
				System.out.println("Susp�s");
			else if (n == 5 || n == 6)
				System.out.println("Aprovat");
			else if (n == 7 || n == 8)
				System.out.println("Notable");
			else
				System.out.println("Excel�lent");
		}
		lector.close();
	}

}
