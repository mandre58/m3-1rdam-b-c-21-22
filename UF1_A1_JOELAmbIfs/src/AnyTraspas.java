import java.util.Scanner;

public class AnyTraspas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner (System.in);
		
		System.out.println("Introdueix l'any: ");
		int any = lector.nextInt();
		
		if (any % 4 == 0 && any % 100 != 0 || any % 400 == 0 ) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
		lector.close();
	}
}